# Contributing to *<PROJECT_NAME>*
First off, thanks for taking the time to contribute!

The following is a set of guidelines for contributing to the *<PROJECT_NAME>* project and its packages, which are hosted in the *Lotus4Her* Organization. 
These are mostly guidelines, not rules. 
Use your best judgment, and feel free to propose changes to this document in a pull request.

## How can I contribute?

### Reporting bugs
This section guides you through submitting a bug report for *<PROJECT_NAME>*. 
Following these guidelines helps maintainers and the community understand your report, reproduce the behavior, and find related reports.

Before creating bug reports, please check the existing list as you might find out that you don't need to create one. 
When you are creating a bug report, please include as many details as possible. 
Fill out the required template, the information it asks for helps us resolve issues faster.

#### Before submitting a bug report
- Check the discussions for a list of common questions and problems.
- Determine which repository the problem should be reported in.
- Perform a cursory search to see if the problem has already been reported. If it has and the issue is still open, add a comment to the existing issue instead of opening a new one.

#### How do I submit a (good) bug report?
Bugs are tracked as issues. 
After you've determined which repository your bug is related to, create an issue on that repository and provide the following information by filling in the template.

Explain the problem and include additional details to help maintainers reproduce the problem:
- Use a clear and descriptive title for the issue to identify the problem.
- Describe the exact steps which reproduce the problem in as many details as possible.
- Provide specific examples to demonstrate the steps.
- Describe the behavior you observed after following the steps and point out what exactly is the problem with that behavior.
- Explain which behavior you expected to see instead and why.
- Include screenshots and animated GIFs which show you following the described steps and clearly demonstrate the problem.
- If you're reporting a crash, include a crash report with a stack trace from the operating system.
- If the problem is related to performance or memory, include a CPU profile capture with your report.
- If the problem wasn't triggered by a specific action, describe what you were doing before the problem happened and share more information using the guidelines below.

Provide more context by answering these questions:
- Can you reproduce the problem in safe mode?
- Did the problem start happening recently (e.g. after updating to a new version) or was this always a problem?
- Can you reproduce the problem in an older version?
- Can you reliably reproduce the issue? If not, provide details about how often the problem happens and under which conditions it normally happens.

## Styleguides

### Git commit messages
+ Use the imperative mood ("Move cursor to..." not "Moves cursor to...")
+ Limit the first line to 72 characters or less
+ Reference issues and pull requests liberally after the first line
+ Consider starting the commit message with an applicable prefix:
  + `docs:` - Adding or updating documentation
  + `feat:` | `feat(42):` - Adding a new feature with id number 42
  + `fix:` | `fix(42):` Fixing a bug, most likely related to an issue with id number 42
  + `refact:` - Refactoring code
  + `clean:` - Indentation changes, brackets alignment, spliting classes into multiple files, etc
  + `test:` - Anything related to testing

### Documentation styleguide
+ Use markdown
  + Use a single `h1` header per file.
  + Use `#` and `##` instead of `=` and `-` for `h1` and `h2` respectfully.
  + Use `-` over `*` and `+` for unordered lists.
  + Use ` ``` ` followed by the code language for code blocks.
  + Use `>` followed by `IMPORTANT`, `NOTE` or `WARNING` to remark a consideration.

## Additional notes
Labels are split into three main namespaces: `priority`, `work` and `type`.
For issues, the `priority` and the `work` labels must aways be provided since they provide a measurement of the work ahead allowing to size and split tasks.
For pull requests (aka merge requests), the `type` label should always be provided to easily identify the code change.

### Issue and pull request labels
| Label | Description |
| :-    | :-          |
| `priority::importance::high` | Issues that might block future work. Uses the Eisenhower Decision Principle. 
| `priority::importance::low` | Issues that won´t block future work. Uses the Eisenhower Decision Principle. 
| `priority::urgency::high` | Issues that require immediate attention. Uses the Eisenhower Decision Principle. 
| `priority::urgency::low` | Issues that do not require immediate attention. Uses the Eisenhower Decision Principle. 
| `work::disorder` | Issues with no clarity of which kind of consistency or work applies. Based of the Cynefin framework. 
| `work::chaotic` | Issues that are "too confusing to wait for a knowledge-based response". Relationship between cause and effect are unclear. Based of the Cynefin framework. 
| `work::complex` | Issues that consists of "unknown unknowns". Relationship between cause and effect can only be deduced in retrospect, and there are no right answers. Based of the Cynefin framework. 
| `work::complicated` | Issues that consists of "known unknowns". Relationship between cause and effect requires analysis or expertise. Based of the Cynefin framework. 
| `work::simple` | Issues that consists of "known knowns". The relationship between cause and effect is clear: if you do X, expect Y. Based of the Cynefin framework. 
| `status::progress` | Issues that have been attended and are being worked towards a solution.
| `status::testing`  | Issues that have been solved and are pending testing and/or approvals.
